# Document Image Processing Pipeline

This project provides a comprehensive pipeline for processing scanned document images. It includes steps for raw image preprocessing, document segmentation, dewarping for perspective correction, and final image enhancement. The goal is to prepare images for better OCR (Optical Character Recognition) performance and readability.

## Project Structure

- `images/`: Contains the raw images of documents.
- `warped_images/`: Contains dewarped images with corrected perspective distortions.
- `processed_images/`: Contains images that have been processed for noise reduction, binarization, and are ready for OCR.

## Pipeline Overview

The processing pipeline consists of the following steps:

1. **Preprocessing**: Enhance raw images for better edge detection and OCR accuracy.
2. **Document Segmentation**: Isolate the document from the background of the image.
3. **Dewarping**: Correct any perspective distortion typically caused by camera angle or paper folds.
4. **Postprocessing**: Apply final touches, such as deblurring, to ensure the highest possible quality for the text.

## How to Run the Pipeline

To execute the pipeline, follow these steps:

1. Ensure you have Python installed on your machine.
2. Install the necessary libraries as specified in `poetry.lock` and `pyproject.toml` by running `poetry install`.
3. Place the images you want to process in the `images/` directory.
4. Run the main script `scanner.py` which will process images and save the results in the respective directories.

```shell
python scanner.py
```

## Example Output

Below are examples of images at different stages in the pipeline:

### Raw Image

![Raw Image](images/image1.jpg)

### Warped Image

![Warped Image](warped_images/image1_warped.jpg)

### Processed Image

![Processed Image](processed_images/image1_processed.jpg)

## Contributions

Contributions to this project are welcome. Please submit a pull request or open an issue to discuss proposed changes.

## Acknowledgements

This project uses techniques and algorithms that are at the forefront of computer vision and AI research. We thank the open-source community for their invaluable contributions.