import sys
import cv2
import os
import numpy as np
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QLabel,
    QVBoxLayout,
    QWidget,
    QFileDialog,
    QScrollArea,
    QHBoxLayout,
)
from PySide6.QtGui import QImage, QPixmap
from PySide6.QtCore import Qt
from imutils.perspective import four_point_transform
import pytesseract

# Configuration for tesseract executable path
pytesseract.pytesseract.tesseract_cmd = (
    "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"
)


# Declare constants for the output size, these can be adjusted
OUTPUT_WIDTH = 595
OUTPUT_HEIGHT = 842


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        # Initialize variables
        self.current_image = None
        self.current_image_path = None

        self.setWindowTitle("Document Scanner and OCR")
        self.setWindowState(Qt.WindowMaximized)

        self.scroll_area = QScrollArea()
        self.image_container = QWidget()
        self.image_layout = QHBoxLayout()

        self.image_label = QLabel("Load an image to start")
        self.image_label.setFixedSize(600, 800)
        self.warped_label = QLabel("Warped image")
        self.warped_label.setFixedSize(600, 800)
        self.processed_label = QLabel("Processed image")
        self.processed_label.setFixedSize(600, 800)

        self.image_layout.addWidget(self.image_label)
        self.image_layout.addWidget(self.warped_label)
        self.image_layout.addWidget(self.processed_label)

        self.image_container.setLayout(self.image_layout)
        self.scroll_area.setWidget(self.image_container)
        self.scroll_area.setWidgetResizable(True)
        self.setCentralWidget(self.scroll_area)

        load_button = QPushButton("Load Image")
        load_button.clicked.connect(self.load_image)

        process_button = QPushButton("Process Image")
        process_button.clicked.connect(self.process_image)

        save_button = QPushButton("Save Images")
        save_button.clicked.connect(self.save_images)

        button_layout = QVBoxLayout()
        button_layout.addWidget(load_button)
        button_layout.addWidget(process_button)
        button_layout.addWidget(save_button)

        self.image_layout.addLayout(button_layout)

        self.current_image = None

    def load_image(self):
        file_path, _ = QFileDialog.getOpenFileName(
            self, "Open Image File", "", "Images (*.png *.jpg *.jpeg *.bmp)"
        )
        if file_path:
            self.current_image_path = file_path  # Store the path of the loaded image
            self.current_image = cv2.imread(file_path)
            self.display_image(self.current_image, self.image_label)

    def pre_processing(self, img):
        """Pre-process the image to prepare it for contour detection."""
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_blur = cv2.fastNlMeansDenoising(img_gray, h=10)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        morph = cv2.morphologyEx(img_blur, cv2.MORPH_CLOSE, kernel, iterations=5)
        img_canny = cv2.Canny(morph, 75, 200)
        kernel = np.ones((5, 5))
        img_dial = cv2.dilate(img_canny, kernel, iterations=1)
        img_thresh = cv2.erode(img_dial, kernel, iterations=1)
        return img_thresh

    def corner_detector(self, img):
        """Detect the corners of the largest contour in the image."""
        contours, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        corners = np.array([])
        max_area = 0
        for cnt in contours:
            area = cv2.contourArea(cnt)
            if area > 6000:
                epsilon = 0.05 * cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, epsilon, True)
                if area > max_area and len(approx) == 4:
                    corners = approx
                    max_area = area
        return corners

    def sort_corners(self, corners):
        """Sort corners to the correct order for perspective transformation."""
        corners = corners.reshape((4, 2))
        sorted_corners = np.zeros((4, 1, 2), np.int32)
        add = corners.sum(1)
        diff = np.diff(corners, axis=1)
        sorted_corners[0] = corners[np.argmin(add)]
        sorted_corners[3] = corners[np.argmax(add)]
        sorted_corners[1] = corners[np.argmin(diff)]
        sorted_corners[2] = corners[np.argmax(diff)]
        return sorted_corners

    def warper(self, img, corners):
        """Perform perspective transformation to get a top-down view of the document."""
        sorted_corners = self.sort_corners(corners)
        pts1 = np.float32(sorted_corners)
        pts2 = np.float32(
            [
                [0, 0],
                [OUTPUT_WIDTH, 0],
                [0, OUTPUT_HEIGHT],
                [OUTPUT_WIDTH, OUTPUT_HEIGHT],
            ]
        )
        matrix = cv2.getPerspectiveTransform(pts1, pts2)
        img_warped = cv2.warpPerspective(img, matrix, (OUTPUT_WIDTH, OUTPUT_HEIGHT))
        return img_warped

    def process_image(self):
        if self.current_image is None:
            return

        # Enhanced preprocessing
        preprocessed = preProcessing(self.current_image.copy())

        # Corner detection and perspective transform
        corners = cornerDetector(preprocessed)
        if corners is not None and len(corners) == 4:
            warped = warper(self.current_image.copy(), corners, width=960, height=1280)
        else:
            warped = self.current_image.copy()  # If no corners, use original image

        # Image processing (binarization in this example)
        processed = self.image_processing(warped)
        self.display_image(warped, self.warped_label)
        self.display_image(processed, self.processed_label)
        self.warped_image = warped
        self.processed_image = processed

    def save_images(self):
        if (
            hasattr(self, "warped_image")
            and hasattr(self, "processed_image")
            and self.current_image_path
        ):
            base_name = os.path.basename(self.current_image_path)
            name, ext = os.path.splitext(base_name)

            # Create directories if they do not exist
            warped_dir = "warped_images"
            processed_dir = "processed_images"
            os.makedirs(warped_dir, exist_ok=True)
            os.makedirs(processed_dir, exist_ok=True)

            # Construct file paths
            warped_path = os.path.join(warped_dir, f"{name}_warped{ext}")
            processed_path = os.path.join(processed_dir, f"{name}_processed{ext}")

            # Save images
            cv2.imwrite(warped_path, self.warped_image)
            cv2.imwrite(processed_path, self.processed_image)
            print(f"Images saved successfully:\n{warped_path}\n{processed_path}")
        else:
            print("No images to save.")

    def image_processing(self, image):
        # Convert to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Apply a combination of median and Gaussian blurring to reduce noise
        # while keeping edges sharp
        blur = cv2.medianBlur(gray, 3)
        blur = cv2.GaussianBlur(blur, (5, 5), 0)

        # Apply adaptive thresholding to get a binary image
        thresh = cv2.adaptiveThreshold(
            blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 9
        )

        # Invert the image if needed
        thresh = cv2.bitwise_not(thresh)

        # Dilate the image to close gaps in lines
        kernel = np.ones((2, 2), np.uint8)
        dilated = cv2.dilate(thresh, kernel, iterations=1)

        # Apply CLAHE (Contrast Limited Adaptive Histogram Equalization)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        equalized = clahe.apply(dilated)

        return equalized

    def scan_detection(self, image):
        # Basic image processing to find contours
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5, 5), 0)
        _, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        contours, _ = cv2.findContours(
            thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )

        # Find the largest contour by area
        largest_contour = max(contours, key=cv2.contourArea, default=None)

        # Approximate the contour to a polygon
        if largest_contour is not None:
            peri = cv2.arcLength(largest_contour, True)
            approx = cv2.approxPolyDP(largest_contour, 0.02 * peri, True)

            # If the polygon has four sides, it could be the paper
            if len(approx) == 4:
                return approx
        return None

    def display_image(self, image, label):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        height, width, channel = image.shape
        bytes_per_line = 3 * width
        q_image = QImage(
            image.data, width, height, bytes_per_line, QImage.Format_RGB888
        )
        pixmap = QPixmap.fromImage(q_image)
        label.setPixmap(
            pixmap.scaled(label.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation)
        )


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
